import { Controller, Get, Param, Post, Query } from '@nestjs/common';
import { DepartmentDto } from 'src/dtos/deparment.dto';
import { LocationDto } from 'src/dtos/location.dto';
import { LocationEntity } from 'src/entities/location.entity';
import { LocationService } from './location.service';

@Controller('location')
export class LocationController {

    constructor(
        private readonly locationService: LocationService
    ){}

    @Post()
    async save(location: LocationDto): Promise<LocationEntity>{
        return await this.locationService.save(location);
    }

    @Get()
    async all( @Query('idDepartment') id:number ): Promise<LocationEntity[]>{
        if(id) {
            // const deparment = new DepartmentDto();
            // deparment.id = id;
            return await this.locationService.findById( id );
        } else {
            return await this.locationService.findAll();
        }
        
    }

}
