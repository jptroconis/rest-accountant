import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DepartmentDto } from 'src/dtos/deparment.dto';
import { LocationDto } from 'src/dtos/location.dto';
import { LocationEntity } from 'src/entities/location.entity';
import { Repository } from 'typeorm';

@Injectable()
export class LocationService {

    constructor(
        @InjectRepository(LocationEntity)  private locationRepository: Repository<LocationEntity>
    ){}

    async save(location: LocationDto): Promise<LocationEntity>{
        return await this.locationRepository.save(location);
    }

    async findAll(): Promise<LocationEntity[]>{
        return await this.locationRepository.find();
    }

    async findById( idDepartment: number ) : Promise<LocationEntity[]> {
        return await this.locationRepository.find({ 
            relations:['department'],
            where: {
                department: { id: idDepartment }
            }
        });
    }

}
