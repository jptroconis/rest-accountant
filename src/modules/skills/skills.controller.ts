import { Body, Controller, Get, Post } from '@nestjs/common';
import { SkillsDto } from 'src/dtos/skills.dto';
import { SkillsEntity } from 'src/entities/skills.entity';
import { SkillsService } from './skills.service';

@Controller('skills')
export class SkillsController {

    constructor(private readonly skillService: SkillsService)
    {}

    @Post()
    async save(@Body() skill: SkillsDto): Promise<SkillsEntity>{
        return await this.skillService.create(skill)
    }

    @Get()
    async all(): Promise<SkillsEntity[]>{
        return await this.skillService.findAll();
    }
}
