import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { SkillsDto } from 'src/dtos/skills.dto';
import { SkillsEntity } from 'src/entities/skills.entity';
import { Repository } from 'typeorm';

@Injectable()
export class SkillsService {

    constructor(
        @InjectRepository(SkillsEntity) private skillRepo: Repository<SkillsEntity>,
    ){}


    async create(skill: SkillsDto): Promise<SkillsEntity>{
        return await this.skillRepo.save(skill);
    }

    async findAll(): Promise<SkillsEntity[]> {
        return await this.skillRepo.find();
    }






}

