import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SkillsEntity } from 'src/entities/skills.entity';
import { SkillsController } from './skills.controller';
import { SkillsService } from './skills.service';


@Module({
    imports:[TypeOrmModule.forFeature([SkillsEntity])],
    controllers: [SkillsController],
    providers: [SkillsService] 
})
export class SkillsModule {}
