import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeLanguagesDto } from 'src/dtos/type-languages.dto';
import { TypeLanguagesEntity } from 'src/entities/type-languages.entity';
import { Repository } from 'typeorm';

@Injectable()
export class TypeLanguageService {

    constructor(
        @InjectRepository(TypeLanguagesEntity) private readonly typeLanguagesRepo: Repository<TypeLanguagesEntity>,
    ){}

    async create(typeIdentity: TypeLanguagesDto): Promise<TypeLanguagesEntity>{
        return await this.typeLanguagesRepo.save(typeIdentity);
    }

    async findAll(): Promise<TypeLanguagesEntity[]> {
        return await this.typeLanguagesRepo.find();
    }
}
