import { Test, TestingModule } from '@nestjs/testing';
import { TypeLanguageService } from './type-language.service';

describe('TypeLanguageService', () => {
  let service: TypeLanguageService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TypeLanguageService],
    }).compile();

    service = module.get<TypeLanguageService>(TypeLanguageService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
