import { Test, TestingModule } from '@nestjs/testing';
import { TypeLanguageController } from './type-language.controller';

describe('TypeLanguageController', () => {
  let controller: TypeLanguageController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TypeLanguageController],
    }).compile();

    controller = module.get<TypeLanguageController>(TypeLanguageController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
