import { Body, Controller, Get, Post } from '@nestjs/common';
import { TypeLanguagesDto } from 'src/dtos/type-languages.dto';
import { TypeLanguagesEntity } from 'src/entities/type-languages.entity';
import { TypeLanguageService } from './type-language.service';

@Controller('type-language')
export class TypeLanguageController {

    constructor(private readonly typeLanguage: TypeLanguageService){}

    @Post()
    async save(@Body() typeIdentity: TypeLanguagesDto): Promise<TypeLanguagesEntity>{
        return await this.typeLanguage.create(typeIdentity);
    }

    @Get()
    async all(): Promise<TypeLanguagesEntity[]>{
        return await this.typeLanguage.findAll();
    }
    
}
