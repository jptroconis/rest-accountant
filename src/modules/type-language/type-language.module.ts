import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TypeLanguagesEntity } from 'src/entities/type-languages.entity';
import { TypeLanguageController } from './type-language.controller';
import { TypeLanguageService } from './type-language.service';

@Module({
  imports: [TypeOrmModule.forFeature([TypeLanguagesEntity])],
  controllers: [TypeLanguageController],
  providers: [TypeLanguageService]
})
export class TypeLanguageModule {}
