import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { PeopleEntity } from 'src/entities/people.entity';
import { Repository } from 'typeorm';
import {PeopleDto} from '../../dtos/people.dto'

@Injectable()
export class PeopleService {

    constructor(
        @InjectRepository(PeopleEntity) private peopleRepo: Repository<PeopleEntity>,
    ){}

        async create(people: PeopleDto): Promise<PeopleEntity>{
            return await this.peopleRepo.save(people);
        }

        async findAll(): Promise<PeopleEntity[]>{
            return await this.peopleRepo.find();
        }

}

