
import { Body, Controller, Get, Post, UseInterceptors } from '@nestjs/common';
import { PeopleDto } from 'src/dtos/people.dto';
import { PeopleEntity } from 'src/entities/people.entity';
import { GeneralInterceptor } from 'src/interceptors/general.interceptor';
import { PeopleService } from './people.service';

@UseInterceptors(GeneralInterceptor)
@Controller('people')
export class PeopleController {
    constructor(private readonly peopleService: PeopleService)
    {}

    @Post()
    async save(@Body() people: PeopleDto): Promise<PeopleEntity>{
        return await this.peopleService.create(people)
    }

    @Get()
    async all(): Promise<PeopleEntity[]>{
        return await this.peopleService.findAll();
    }



}
