import { Test, TestingModule } from '@nestjs/testing';
import { TypeIdentityService } from './type-identity.service';

describe('TypeIdentityService', () => {
  let service: TypeIdentityService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TypeIdentityService],
    }).compile();

    service = module.get<TypeIdentityService>(TypeIdentityService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
