import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { GenreEntity } from 'src/entities/genre.entity';
import { TypeIdentityEntity } from 'src/entities/type-identity.entity';
import { TypeIdentityController } from './type-identity.controller';
import { TypeIdentityService } from './type-identity.service';

@Module({
  imports: [TypeOrmModule.forFeature([TypeIdentityEntity, GenreEntity])],
  controllers: [TypeIdentityController],
  providers: [TypeIdentityService]
})
export class TypeIdentityModule {}
