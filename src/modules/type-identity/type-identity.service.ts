import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeIdentityDto } from 'src/dtos/type-identity.dto';
import { GenreEntity } from 'src/entities/genre.entity';
import { TypeIdentityEntity } from 'src/entities/type-identity.entity';
import { Repository } from 'typeorm';

@Injectable()
export class TypeIdentityService {

    constructor(
        @InjectRepository(TypeIdentityEntity) private typeIdentityRepo: Repository<TypeIdentityEntity>,
        @InjectRepository(GenreEntity) private genreRepo: Repository<GenreEntity>,
    ){}

    async create(typeIdentity: TypeIdentityDto): Promise<TypeIdentityEntity>{
        return await this.typeIdentityRepo.save(typeIdentity);
    }

    async findAll(): Promise<TypeIdentityEntity[]> {
        return await this.typeIdentityRepo.find();
    }

    async findAllGenre(): Promise<TypeIdentityEntity[]> {
        return await this.genreRepo.find();
    }



}
