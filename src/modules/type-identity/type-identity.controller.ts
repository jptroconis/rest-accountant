import { Body, Controller, Get, Post } from '@nestjs/common';
import { TypeIdentityDto } from 'src/dtos/type-identity.dto';
import { GenreEntity } from 'src/entities/genre.entity';
import { TypeIdentityEntity } from 'src/entities/type-identity.entity';
import { TypeIdentityService } from './type-identity.service';

@Controller('type-identity')
export class TypeIdentityController {

    constructor(private readonly typeIdentityService: TypeIdentityService){}


    @Post()
    async save(@Body() typeIdentity: TypeIdentityDto): Promise<TypeIdentityEntity>{
        return await this.typeIdentityService.create(typeIdentity);
    }

    @Get()
    async all(): Promise<TypeIdentityEntity[]>{
        return await this.typeIdentityService.findAll();
    }

    @Get('genre')
    async allGender(): Promise<GenreEntity[]>{
        return await this.typeIdentityService.findAllGenre();
    }

}
