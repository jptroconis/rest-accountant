import { Test, TestingModule } from '@nestjs/testing';
import { TypeIdentityController } from './type-identity.controller';

describe('TypeIdentityController', () => {
  let controller: TypeIdentityController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TypeIdentityController],
    }).compile();

    controller = module.get<TypeIdentityController>(TypeIdentityController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
