import { Body, Controller, Get, Post } from '@nestjs/common';
import { UserDto } from 'src/dtos/user.dto';
import { UserEntity } from 'src/entities/user.entity';
import { UsersService } from './users.service';


@Controller('users')
export class UsersController {
  constructor(private readonly userService: UsersService) {}

  @Post('save')
  async save(@Body() user: UserDto): Promise<UserEntity> {
    return await this.userService.create(user);
  }

  @Get()
  async all(): Promise<UserEntity[]> {
    return await this.userService.findAll();
  }
}
