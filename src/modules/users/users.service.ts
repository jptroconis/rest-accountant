/* eslint-disable prettier/prettier */
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserDto } from 'src/dtos/user.dto';
import { UserEntity } from 'src/entities/user.entity';
import { Repository } from 'typeorm';

@Injectable()
export class UsersService {

    constructor(
        @InjectRepository(UserEntity) private userRepo: Repository<UserEntity>,
    ) {}

    async create(user: UserDto): Promise<UserEntity> {
        return await this.userRepo.save(user);
    }

    async findAll(): Promise<UserEntity[]> {
        return await this.userRepo.find();
    }
}
