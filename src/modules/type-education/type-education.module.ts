import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TypeEducationEntity } from 'src/entities/type-education.entity';
import { TypeEducationController } from './type-education.controller';
import { TypeEducationService } from './type-education.service';

@Module({
    imports:[TypeOrmModule.forFeature([TypeEducationEntity])],
    controllers: [TypeEducationController],
    providers: [TypeEducationService]
})
export class TypeEducationModule {}
