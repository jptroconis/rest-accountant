import { Body, Controller, Get, Post } from '@nestjs/common';
import { TypeEducationDto } from 'src/dtos/type-education.dto';
import { TypeEducationEntity } from 'src/entities/type-education.entity';
import { TypeEducationService } from './type-education.service';

@Controller('type-education')
export class TypeEducationController {

    constructor(private readonly typeEducationService: TypeEducationService)
    {}

    @Post()
    async save(@Body() typeEducation: TypeEducationDto): Promise<TypeEducationEntity>{
        return await this.typeEducationService.create(typeEducation)
    }

    @Get()
    async all(): Promise<TypeEducationEntity[]>{
        return await this.typeEducationService.findAll();
    }

}
