import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeEducationDto } from 'src/dtos/type-education.dto';
import { TypeEducationEntity } from 'src/entities/type-education.entity';
import { Repository } from 'typeorm';

@Injectable()
export class TypeEducationService {

    constructor(
        @InjectRepository(TypeEducationEntity) private typeEducationRepo: Repository<TypeEducationEntity>,
    ) {}

    async create(typeEducation: TypeEducationDto): Promise<TypeEducationEntity> {
        return await this.typeEducationRepo.save(typeEducation);
    }

    async findAll(): Promise<TypeEducationEntity[]> {
        return await this.typeEducationRepo.find();
    }
}
