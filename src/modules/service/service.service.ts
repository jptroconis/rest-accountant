import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ServiceDto } from 'src/dtos/service.dto';
import { ServiceEntity } from 'src/entities/service.entity';
import { Repository } from 'typeorm';

@Injectable()
export class ServiceService {

    constructor(
        @InjectRepository(ServiceEntity) private serviceRepo: Repository<ServiceEntity>,
    ) {}

    async create(service: ServiceDto): Promise<ServiceEntity> {
        return await this.serviceRepo.save(service);
    }

    async findAll(): Promise<ServiceEntity[]> {
        return await this.serviceRepo.find();
    }

}
