import { Body, Controller, Get, Post } from '@nestjs/common';
import { ServiceDto } from 'src/dtos/service.dto';
import { ServiceEntity } from 'src/entities/service.entity';
import { ServiceService } from './service.service';

@Controller('service')
export class ServiceController {

    constructor(private readonly serviceService: ServiceService)
    {}

    @Post()
    async save(@Body() service: ServiceDto): Promise<ServiceEntity>{
        return await this.serviceService.create(service)
    }

    @Get()
    async all(): Promise<ServiceEntity[]>{
        return await this.serviceService.findAll();
    }
}
