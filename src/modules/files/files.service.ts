import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FileDto } from 'src/dtos/file.dto';
import { FileEntity } from 'src/entities/file.entity';
import { Repository } from 'typeorm';

@Injectable()
export class FilesService {

    constructor(
        @InjectRepository(FileEntity) private fileRepo : Repository<FileEntity>
    ){}
    
    async create(file: FileDto): Promise<FileEntity>{
        return await this.fileRepo.save(file);
    }

    async findAll(): Promise<FileEntity[]>{
        return await this.fileRepo.find();
    }

    async findbyId(id: string): Promise<FileEntity>{
        return await this.fileRepo.findOne(id);
    }
    

    async delete(id: string) {
        return await this.fileRepo.delete(id);
    }
}
