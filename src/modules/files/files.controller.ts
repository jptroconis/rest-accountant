import { Controller, Get, HttpException, HttpStatus, Param, Post, UploadedFile, UseInterceptors } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { request } from 'express';
import { FileDto } from 'src/dtos/file.dto';
import { FileEntity } from 'src/entities/file.entity';
import { FilesService } from './files.service';
const fs = require('fs')
const imgbbUploader = require('imgbb-uploader');

@Controller('files')
export class FilesController {

    constructor(
        private readonly fileService: FilesService,
    ){}


    @Post('upload/:id')
    @UseInterceptors(FileInterceptor('photo', {
        dest: './uploads'
    }))
    async upload(@UploadedFile() file, @Param() params) {
        try {
            // console.log(file);
            // console.log('id =>', params.id);
            const response = await imgbbUploader(
                '9a462d88f0fbea8a360579d544ffab3f',
                file.path
            )
            fs.unlinkSync(file.path)
            const fileDto = new FileDto();
            fileDto.id = params.id;
            fileDto.display_url = response.display_url;
            fileDto.json_data = JSON.stringify(response);
            await this.fileService.create(fileDto);
            return response;
        } catch( error ) {
            console.log('error', error);
            throw new HttpException({
                status: HttpStatus.BAD_REQUEST,
                error: 'Archivo invalido'
            }, HttpStatus.BAD_REQUEST)
        }
    }

    @Get('file/:id')
    async file( @Param() params ) : Promise<FileEntity> {
        return await this.fileService.findbyId(params.id);
    }
}
