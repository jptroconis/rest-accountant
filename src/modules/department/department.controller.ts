import { Body, Controller, Get, Post } from '@nestjs/common';
import { DepartmentDto } from 'src/dtos/deparment.dto';
import { DepartmentEntity } from 'src/entities/department.entity';
import { DepartmentService } from './department.service';
import * as geoJson from '../../utils/geo.json';
import { LocationDto } from 'src/dtos/location.dto';

@Controller('department')
export class DepartmentController {

    constructor(
        private readonly departmentService: DepartmentService,
    ){}

    @Post()
    async save(@Body() department: DepartmentDto): Promise<DepartmentEntity>{
        return await this.departmentService.create(department);
    }

    @Get()
    async all(): Promise<DepartmentEntity[]> {
        return await this.departmentService.findAll();
    }

    @Get('deleteAll')
    deleteall(){
        for(let i=0 ; i<=geoJson.length ; i++){
            this.departmentService.delete(i);
        }
        return "Completed all delete";
    }

    @Get('fill')
    async fill(){

        for (let i=0; i<geoJson.length; i++){
            
            const deparment : DepartmentDto = new DepartmentDto();
            deparment.name = geoJson[i].departamento;
            deparment.locations = [];
            
            for(let j=0; j<geoJson[i].ciudades.length ; j++ ){
                const location : LocationDto = new LocationDto();
                location.city = geoJson[i].ciudades[j];
                deparment.locations.push(location);
                // console.log('departamento ', entity.id, + ' ', entity.name)
            }
            await this.departmentService.create(deparment);
        }

            
        
        
       // geoJson.forEach(el => this.save(el.departamento));

        return "completed";
    }

    





}
