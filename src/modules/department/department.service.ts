import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DepartmentDto } from 'src/dtos/deparment.dto';
import { DepartmentEntity } from 'src/entities/department.entity';
import { Repository } from 'typeorm';

@Injectable()
export class DepartmentService {

    constructor(
        @InjectRepository(DepartmentEntity) private departmentRepo : Repository<DepartmentEntity>
    ){}

    async create(department: DepartmentDto): Promise<DepartmentEntity>{
        return await this.departmentRepo.save(department);
    }

    async findAll(): Promise<DepartmentEntity[]>{
        return await this.departmentRepo.find();
    }

    async delete(id: number) {
        return await this.departmentRepo.delete(id);
    }


}

