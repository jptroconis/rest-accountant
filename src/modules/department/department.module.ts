import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DepartmentEntity } from 'src/entities/department.entity';
import { LocationEntity } from 'src/entities/location.entity';
import { LocationService } from '../location/location.service';
import { DepartmentController } from './department.controller';
import { DepartmentService } from './department.service';

@Module({
  imports: [TypeOrmModule.forFeature( [DepartmentEntity, LocationEntity] ) ],
  controllers: [DepartmentController],
  providers: [DepartmentService, LocationService]
})
export class DepartmentModule {}
