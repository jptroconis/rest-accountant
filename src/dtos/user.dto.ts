import { RolPeopleDto } from "./rol-people.dto";

export class UserDto {
  id: number;
  user: string;
  password: string;
  rolPeople : RolPeopleDto;

}
