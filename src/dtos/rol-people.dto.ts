import { PeopleDto } from "./people.dto";
import { RolAcconuntantDto } from "./rol-accountant.dto";

export class RolPeopleDto{
    id : number;
    people: PeopleDto;
    rolAccountant: RolAcconuntantDto;
}