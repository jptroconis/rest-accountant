export class GenreDto {
    id: number;
    code: string;
    name: string;
}