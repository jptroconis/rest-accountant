export class FileDto {
    id: string;
    json_data: string;
    display_url: string;
}
