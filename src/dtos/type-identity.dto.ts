export class TypeIdentityDto{
    id: number;
    code: string;
    name: string;
}