export class ServiceDto{
    id: number;
    name: string;
    description: string;
}