import { GenreDto } from "./genre.dto";

export class PublicPeopleDto{
    id: number;
    identification: string;
    name: string;
    lastname: string;
    genre: GenreDto;
}