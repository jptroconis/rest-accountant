import { DepartmentDto } from "./deparment.dto";


export class LocationDto{
    id: number;
    city: string;
    deparment: DepartmentDto
}