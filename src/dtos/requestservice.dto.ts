import { PeopleDto } from "./people.dto";
import { PublicPeopleDto } from "./public-people.dto";
import { ServiceDto } from "./service.dto";
import { TypeIdentityDto } from "./type-identity.dto";

export class RequestServiceDto{

    id: number;
    code: string;
    beginDate: Date;
    endDate: Date;
    statusService: string;
    description: string;
    
    price: number;
    publicPeople: PublicPeopleDto;
    people: PeopleDto;
    service: ServiceDto;
    typeIdentity: TypeIdentityDto;

}