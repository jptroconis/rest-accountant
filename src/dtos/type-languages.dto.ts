export class TypeLanguagesDto{
    id: number;
    code: string;
    name: string;
}