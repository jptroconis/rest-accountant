import { ServiceDto } from "./service.dto";
import { SkillsDto } from "./skills.dto";
import { TypeIdentityDto } from "./type-identity.dto";
import { GenreDto } from "./genre.dto";
import { TypeLanguagesDto } from "./type-languages.dto";

export class PeopleDto{
    id: number;
    identification: string;
    name: string;
    lastname: string;
    genre: GenreDto;
    email: string;
    url_profile: string;
    typeIdentity : TypeIdentityDto;
    skills: SkillsDto[];
    type_languages: TypeLanguagesDto[];
    services: ServiceDto[];
    
}
