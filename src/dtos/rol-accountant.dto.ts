import { LocationDto } from "./location.dto";
import { TypeEducationDto } from "./type-education.dto";

export class RolAcconuntantDto{
    id: number;
    professionalCard: string;
    graduationDate: Date;
    experienceYears: number;
    school: string;
    phone: number;
    description: string;
    typeEducation: TypeEducationDto;
    location: LocationDto;
}
