import { LocationDto } from "./location.dto";

export class DepartmentDto{
    id: number;
    name: string;
    locations: LocationDto[];
}