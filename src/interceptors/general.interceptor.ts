import { CallHandler, ExecutionContext, Injectable, NestInterceptor, Response } from '@nestjs/common';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

interface Response<T> {
  statusCode: number,
  message: string,
  data: T
}

@Injectable()
export class GeneralInterceptor<T> implements NestInterceptor<T, Response<T>> {

  intercept(
    context: ExecutionContext, 
    next: CallHandler
  ): Observable<any> {
    return next
    .handle()
    .pipe(
      map( (data ) => ({
        statusCode: context.switchToHttp().getResponse().statusCode,
        message: data.message,
        data
      }) ),
      catchError(err => {
        throw err;
      }),
    );
  }

}
