import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { DepartmentModule } from './modules/department/department.module';
import { LocationModule } from './modules/location/location.module';
import { PeopleModule } from './modules/people/people.module';
import { ServiceModule } from './modules/service/service.module';
import { SkillsModule } from './modules/skills/skills.module';
import { TypeEducationModule } from './modules/type-education/type-education.module';
import { TypeIdentityModule } from './modules/type-identity/type-identity.module';
import { TypeLanguageModule } from './modules/type-language/type-language.module';
import { UsersModule } from './modules/users/users.module';
import { APP_INTERCEPTOR } from '@nestjs/core';
import { GeneralInterceptor } from './interceptors/general.interceptor';
import { FilesModule } from './modules/files/files.module';


@Module({
  imports: [
    TypeOrmModule.forRoot({
      
      type: 'sqlite',
      database: 'db.sql',
      entities: [__dirname + '/**/*.entity{.ts,.js}'],
      synchronize: true,
      
      /*
      type: 'mysql',
      host: 'remotemysql.com',
      port: 3306,
      username: 'A8fzN9wH2R',
      password: '7UXyBxVPEs',
      database: 'A8fzN9wH2R',
      entities: [],
      synchronize: true
      */
    }),
    UsersModule,
    PeopleModule,
    TypeIdentityModule,
    LocationModule,
    DepartmentModule,
    SkillsModule,
    ServiceModule,
    TypeEducationModule,
    TypeLanguageModule,
    FilesModule,
  ],
  controllers: [AppController],
  providers: [
    AppService, 
    {
      provide: APP_INTERCEPTOR,
      useClass: GeneralInterceptor
    }
  ],
})
export class AppModule {}
