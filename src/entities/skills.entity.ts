import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity({
    name: "skills"
})
export class SkillsEntity{

    @PrimaryGeneratedColumn() id: number;
    @Column() name: string;

}