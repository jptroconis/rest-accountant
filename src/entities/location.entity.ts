import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import {DepartmentEntity} from "./department.entity"

@Entity({
    name: "location"
})
export class LocationEntity{

    @PrimaryGeneratedColumn() id: number;
    
    @Column() city: string;

    @ManyToOne(() => DepartmentEntity, department => department.locations)
    department: DepartmentEntity;
}