import { Column, Double, Entity, JoinColumn, ManyToOne, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import {PeopleEntity} from './people.entity';
import {ServiceEntity} from './service.entity';
import {PublicPeopleEntity} from './public-people.entity';
import {TypeIdentityEntity} from './type-identity.entity';

@Entity({
    name: "request_service"
})
export class RequestServiceEntity{

    @PrimaryGeneratedColumn() id: number;
    @Column({ type: 'date' }) beginDate: Date;
    @Column({ type: 'date' }) endDate: Date;
    @Column() statusService: string;
    @Column({ nullable: true }) description: string;
    @Column({
        type: 'double',
        nullable: true
    }) 
    price: Double;
    
    @ManyToOne(() => PublicPeopleEntity, publicPeople => publicPeople.id, { cascade: true })
    @JoinColumn({name: "public_people_id"})
    publicPeople: PublicPeopleEntity;
    
    @ManyToOne(() => PeopleEntity, people => people.id ,{ cascade: true })
    @JoinColumn({name: "people_id"})
    people: PeopleEntity;

    @ManyToOne(() => ServiceEntity, service => service.id, { cascade: true })
    @JoinColumn({name: "service_id"})
    service: ServiceEntity;

}