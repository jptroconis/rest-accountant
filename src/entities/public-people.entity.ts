import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { GenreEntity } from "./genre.entity";
import { TypeIdentityEntity } from "./type-identity.entity";

@Entity({
    name: "public_people"
})
export class PublicPeopleEntity{

    @PrimaryGeneratedColumn() id: number;
    @Column() identification: string;
    @Column() name: string;
    @Column() lastname: string;

    @ManyToOne(() => GenreEntity, genre => genre.id, { cascade: true })
    @JoinColumn({name: "genre_id"})
    genre: GenreEntity;

    @ManyToOne(() => TypeIdentityEntity, typeIdentity => typeIdentity.id)
    @JoinColumn({name: "type_identity_id"})
    typeIdentity: TypeIdentityEntity;
}