import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity({
    name: "genre"
})
export class GenreEntity {

    @PrimaryGeneratedColumn() id: number;
    @Column() code: string;
    @Column() name: string;
}