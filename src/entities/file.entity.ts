import { Column, Entity, PrimaryColumn, PrimaryGeneratedColumn } from 'typeorm';

@Entity({
  name: 'file'
})
export class FileEntity {

  @PrimaryColumn() id: string;
  @Column({ type: 'text' }) json_data: string;
  @Column() display_url: string;

}

