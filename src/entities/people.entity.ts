import { Column, Entity, JoinColumn, JoinTable, ManyToMany, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import {TypeIdentityEntity} from './type-identity.entity'
import {SkillsEntity} from './skills.entity';
import {TypeLanguagesEntity} from './type-languages.entity';
import {ServiceEntity} from './service.entity'
import { GenreEntity } from "./genre.entity";

@Entity({
    name: "people"
})
export class PeopleEntity {

    @PrimaryGeneratedColumn() id: number;
    @Column() identification: string;
    @Column() name: string;
    @Column() lastname: string;
    @Column() email: string;
    @Column() url_profile: string;

    @ManyToOne(() => TypeIdentityEntity, typeIdentity => typeIdentity.id, { cascade: true })
    @JoinColumn({name: "type_identity_id"})
    typeIdentity : TypeIdentityEntity;

    @ManyToOne(() => GenreEntity, genre => genre.id, { cascade: true })
    @JoinColumn({name: "genre_id"})
    genre: GenreEntity;

    
    @ManyToMany(() => SkillsEntity, { cascade: true } )
    @JoinTable({
        name: "people_skills",
        joinColumn:{
            name: "people_id",
            referencedColumnName: "id"
        },inverseJoinColumn:{
            name:"skills_id",
            referencedColumnName: "id",
        }
    })
    skills: SkillsEntity[];

    
    @ManyToMany(() => TypeLanguagesEntity, { cascade: true })
    @JoinTable({
        name:"people_languages",
        joinColumn:{
            name: "people_id",
            referencedColumnName: "id"
        },inverseJoinColumn:{
            name:"type_languages_id",
            referencedColumnName: "id"
        }
    })
    type_languages: TypeLanguagesEntity[];


    @ManyToMany(() => ServiceEntity, { cascade: true } )
    @JoinTable({
        name:"people_service",
        joinColumn:{
            name: "people_id",
            referencedColumnName: "id"
        },inverseJoinColumn:{
            name:"service_id",
            referencedColumnName: "id"
        }
    })
    services: ServiceEntity[];

    
}
