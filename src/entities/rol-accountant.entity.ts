import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import {TypeEducationEntity} from './type-education.entity'
import {LocationEntity} from './location.entity'

@Entity({
    name: "rol_accountant",
})
export class RolAccountantEntity {

    @PrimaryGeneratedColumn() id: number;
    @Column() professionalCard: string;
    @Column({ type: 'date' }) graduationDate: Date;
    @Column() experienceYears: number;
    @Column({ nullable: true }) description: string;
    @Column({ nullable: true }) school: string;
    @Column({ nullable: true }) phone: number;

    @ManyToOne(() => TypeEducationEntity, typeEducation => typeEducation.id, { cascade: true })
    @JoinColumn({name: "type_education_id"})
    typeEducation: TypeEducationEntity;
    

    @ManyToOne(() => LocationEntity, location => location.id, { cascade: true })
    @JoinColumn({name: "location_id"})
    location: LocationEntity;

}
