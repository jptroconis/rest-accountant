import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity({name : 'type_languages'})
export class TypeLanguagesEntity{

    @PrimaryGeneratedColumn() id: number;
    @Column() name: string;

}