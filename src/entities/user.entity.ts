import { Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { RolPeopleEntity } from './rol-people.entity';

@Entity({
  name: 'usuario'
})
export class UserEntity {

  @PrimaryGeneratedColumn() id: number;
  @Column( { unique: true } ) user: string;
  @Column() password: string;

  @OneToOne(() => RolPeopleEntity, { cascade: true  })
  @JoinColumn({name: "rol_people_id", referencedColumnName: ''})
  rolPeople : RolPeopleEntity;


}

