import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity({
    name: "type_education"
})
export class TypeEducationEntity{

    @PrimaryGeneratedColumn() id: number;
    @Column() name: string;

}