import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity({
    name: "type_identity"
})
export class TypeIdentityEntity{

    @PrimaryGeneratedColumn() id: number;
    @Column() code: string;
    @Column() name: string;
}