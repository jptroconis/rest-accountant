import { Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import {PeopleEntity} from './people.entity'
import {RolAccountantEntity} from './rol-accountant.entity'

@Entity({
    name: "rol_people"
})
export class RolPeopleEntity {

    @PrimaryGeneratedColumn()
    id : number;

    @OneToOne(() => PeopleEntity, { cascade: true })
    @JoinColumn({name: "people_id"})
    people: PeopleEntity;

    @OneToOne(() => RolAccountantEntity, { cascade: true })
    @JoinColumn({name: "rol_accountant_id"})
    rolAccountant: RolAccountantEntity;

}