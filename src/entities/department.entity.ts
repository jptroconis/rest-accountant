import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import {LocationEntity} from "./location.entity";

@Entity({
    name: "department"
})
export class DepartmentEntity{

    @PrimaryGeneratedColumn() id: number;
    @Column() name: string;

    @OneToMany(() => LocationEntity, location => location.department, { cascade: true })
    locations: LocationEntity[];
}