import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity({
    name: "service"
})
export class ServiceEntity{

    @PrimaryGeneratedColumn() id: number;
    @Column() name: string;
    @Column({ nullable: true }) description: string;
}