import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  getHello() {
    return {
      name: 'rest-accountant',
      version: '0.0.1',
      description: 'An REST API with NESTJS',
      author: 'jptroconis',
    };
  }
}
